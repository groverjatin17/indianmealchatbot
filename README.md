# How to build and run this project

1. Firstly Clone or Download the .zip file of the project
2. Then run npm install in the root folder where the package.json is included. This will install all the dependencies.
3. After that execute npm start to run the project on your local machine
4. Once the dependencies are installed open this link. http://localhost:3002/
5. Now you can chat with the chatbot and order a meal.
