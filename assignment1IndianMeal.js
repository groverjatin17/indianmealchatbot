const Order = require("./assignment1Order");

const OrderState = Object.freeze({
    WELCOMING: Symbol("welcoming"),
    BREADTYPE: Symbol("breadType"),
    CURRYTYPE: Symbol("curryType"),
    CURRYSIZE: Symbol("currySize"),
    CURRYSPICE: Symbol("currySpice"),
    DRINKS: Symbol("drinks"),
});

module.exports = class IndianMeal extends Order {
    constructor() {
        super();
        this.stateCur = OrderState.WELCOMING;
        this.breadtype = "";
        this.curryType = "";
        this.currySize = "";
        this.currySpice = "";
        this.drinks = "";
    }
    handleInput(sInput) {
        let aReturn = [];
        switch (this.stateCur) {
            case OrderState.WELCOMING:
                this.stateCur = OrderState.BREADTYPE;
                aReturn.push(
                    "Welcome to Indian Takeout Store. Please pick what type of Bread and Curry would you like to have."
                );
                aReturn.push(
                    "We serve Naans, Rumali Roti, Paranthas and Tandoori Roti.What Type of Bread would you like?"
                );
                break;
            case OrderState.BREADTYPE:
                this.stateCur = OrderState.CURRYTYPE;
                this.breadtype = sInput;
                aReturn.push("Excellent!!, Thats a Great Choice.");
                aReturn.push(
                    "Now what type of Curry would you like to go along with it."
                );
                break;
            case OrderState.CURRYTYPE:
                this.stateCur = OrderState.CURRYSIZE;
                this.curryType = sInput;
                aReturn.push("Great!!!");
                aReturn.push(
                    "Now what size would you like? Regular or Large??."
                );
                break;
            case OrderState.CURRYSIZE:
                this.stateCur = OrderState.CURRYSPICE;
                this.currySize = sInput;
                aReturn.push(
                    "Now do you like your curry to be Mildy or Extra spicy?"
                );
                break;
            case OrderState.CURRYSPICE:
                this.stateCur = OrderState.DRINKS;
                this.currySpice = sInput;
                aReturn.push(
                    "Thank you. Would you like any drinks to go with your order?"
                );
                break;
            case OrderState.DRINKS:
                this.isDone(true);
                if (sInput.toLowerCase() != "no") {
                    this.drinks = sInput;
                }
                aReturn.push(
                    `Thank you for your order of ${this.breadtype} with ${this.curryType} of ${this.currySize} size that is ${this.currySpice} spicy.`
                );
                if (this.drinks) {
                    aReturn.push(`And we will send complimentary drinks too`);
                }
                let d = new Date();
                d.setMinutes(d.getMinutes() + 20);
                aReturn.push(`Please pick it up at ${d.toTimeString()}`);
                break;
        }
        return aReturn;
    }
};
